<nav class="navbar navbar-expand-lg navbar-light bg-secondary" style="margin-bottom: 40px;">
    <a class="navbar-brand" href="#">Stronka</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="#"></a>
        </li>
    </ul>
    <?php if (!isset($_SESSION['user'])) { ?>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-success my-2 my-sm-0 mr-1" href="../views/loginPage.php">Logowanie</a>
            <a class="btn btn-success my-2 my-sm-0" href="../views/registerPage.php">Rejestracja</a>
        </form>
    <?php } elseif (!$perm->checkAdmin($_SESSION['user']['login'])) { ?>
        <ul class="navbar-nav my-2 my-lg-0 mr-3">
            <li class="nav-item dropdown my-2 my-lg-0">
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Moje konto
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="../views/mainPage.php">Mój profil</a>
                        <a class="dropdown-item"
                           href="../views/editPage.php?option=edit&login=<?= $_SESSION['user']['login'] ?>">Edytuj
                            dane</a>
                        <a class="dropdown-item"
                           href="../Controllers/userController.php?option=delete&login=<?= $_SESSION['user']['login'] ?>">Usuń
                            konto</a>
                        <a class="dropdown-item" href="../views/chatPage.php">Inbox</a>
                        <a class="dropdown-item" href="../Controllers/userController.php?option=logout">Wyloguj</a>
                    </div>
                </div>
            </li>
        </ul>
    <?php } else { ?>

        <ul class="navbar-nav my-2 my-lg-0 mr-3">
            <li class="nav-item dropdown my-2 my-lg-0">
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Moje konto
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="../views/mainPage.php">Mój profil</a>
                        <a class="dropdown-item"
                           href="../views/editPage.php?option=edit&login=<?= $_SESSION['user']['login'] ?>">Edytuj
                            dane</a>
                        <a class="dropdown-item"
                           href="../Controllers/userController.php?option=delete&login=<?= $_SESSION['user']['login'] ?>">Usuń
                            konto</a>
                        <a class="dropdown-item" href="../views/adminPanel.php">Użytkownicy</a>
                        <a class="dropdown-item" href="../views/chatPage.php">Inbox</a>
                        <a class="dropdown-item" href="../Controllers/userController.php?option=logout">Wyloguj</a>
                    </div>
                </div>
            </li>
        </ul>

    <?php } ?>
</nav>


