<?php
if (isset($_COOKIE['session'])) {
    @session_id($_COOKIE['session']);
}
session_start();
$id = session_id();
setcookie('session', $id);
require_once "Models/Database.class.php";
require_once "Models/User.class.php";
require_once "API/API.php";
require_once "Services/UserService.php";
require_once "Models/UserDAO.class.php";
require_once "API/UserAPI.php";
require_once "API/ChatAPI.php";
require_once "Models/Validator.php";
require_once "Models/Permission.php";
$perm = new Permission();
include_once "navbar.php";

