<?php

class UserAPI extends API
{


    public function editUser($status, $icon)
    {
        $fields = array(
            'login' => $this->login,
            'key' => $this->key,
            'status' => $status,
            'icon' => $icon,
        );
        $this->setOpt("http://tank.iai-system.com/api/user/edit", $fields);

        return $this->checkIfArray();

    }

    public function addUser($password)
    {

        $fields = array(
            "login" => $this->login,
            "password" => $password,
        );
        $this->setOpt("http://tank.iai-system.com/api/user/add", $fields);

        return $this->checkIfArray();
    }

    public function verify()
    {

        $fields = array(
            'login' => $this->login,
            'key' => $this->key,
        );
        $this->setOpt("http://tank.iai-system.com/api/user/verify", $fields);

        return $this->checkIfArray();
    }

    public function getAll()
    {

        $this->setOpt("http://tank.iai-system.com/api/user/getAll", $fields = []);
        if ($this->checkIfArray()) {
            return $this->getResult();
        } else {
            return [];
        }

    }

    public function getUser($login)
    {

        $users = $this->getAll();
        foreach ($users as $user) {
            if ($user['login'] == $login) {
                return array(
                    'login' => $login,
                    'status' => $user['status'],
                    'icon' => $user['icon'],
                );

            }
        }
        return [];

    }
    public function logout(){
        $fields = array(
            'login' => $this->login,
            'key' => $this->key,
            'status' => "offline",
        );
        $this->setOpt("http://tank.iai-system.com/api/user/edit", $fields);

        return $this->checkIfArray();
    }


}