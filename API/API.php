<?php


class API
{
    protected $login;
    protected $key;
    private $result;


    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    function setOpt($url, $fields)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $this->curlExec($ch);

        curl_close($ch);

    }


    function checkIfArray()
    {
        if (is_array($this->result)) {
            return true;
        } else {
            return false;
        }
    }

    function curlExec($ch)
    {
        $zm = curl_exec($ch);
        if (!$zm) {
            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }
        $this->result = json_decode($zm, true);
    }
}