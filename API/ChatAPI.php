<?php


class ChatAPI extends API
{

    function __construct()
    {
        $this->key = $_SESSION['user']['key'];
        $this->login = $_SESSION['user']['login'];
    }

    function getActive()
    {
        $fields = array(
            "login" => $this->login,
            "key" => $this->key,
        );
        $this->setOpt("http://tank.iai-system.com/api/chat/getActive", $fields);
        if ($this->checkIfArray()) {
            return $this->getResult();
        } else {
            return [];
        }

    }

    function createChat($name)
    {
        $fields = array(
            "login" => $this->login,
            "key" => $this->key,
            "name" => $name,
        );
        $this->setOpt("http://tank.iai-system.com/api/chat/create", $fields);
        return $this->checkIfArray();

    }

    function joinToChat($user, $chat_id)
    {
        $fields = array(
            "login" => $this->login,
            "key" => $this->key,
            "user" => $user,
            "chat_id" => $chat_id,
        );
        $this->setOpt("http://tank.iai-system.com/api/chat/join", $fields);
        return $this->checkIfArray();


    }

    function leaveChat($chat_id)
    {
        $fields = array(
            "login" => $this->login,
            "key" => $this->key,
            "chat_id" => $chat_id,
        );
        $this->setOpt("http://tank.iai-system.com/api/chat/leave", $fields);
        return $this->checkIfArray();


    }

    function sendMessage($chat_id, $message)
    {

        $fields = array(
            "login" => $this->login,
            "key" => $this->key,
            "chat_id" => $chat_id,
            "message" => $message,
        );
        $this->setOpt("http://tank.iai-system.com/api/chat/send", $fields);
        return $this->checkIfArray();

    }

    function getMessage()
    {

        if (isset($_SESSION['lastMessage'])) {
            $fields = array(
                "login" => $this->login,
                "key" => $this->key,
                "last_id" => intval($_SESSION['lastMessage']),
            );

        } else {
            $fields = array(
                "login" => $this->login,
                "key" => $this->key,
            );
        }
        if (isset($_SESSION['lastMessage'])) {
            unset($_SESSION['lastMessage']);
        }
        $this->setOpt("http://tank.iai-system.com/api/chat/get", $fields);
        if ($this->checkIfArray()) {
            return $this->getResult();
        } else {
            return [];
        }

    }
}