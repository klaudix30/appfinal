<?php require_once "../config.php";
if(!$perm->checkLogin()){
    header('Location: ../views/loginPage.php');
    die;
}
$api = new UserAPI();
$users = $api->getUser($_SESSION['user']['login']);
?>
<html>

<head>
    <title>Strona główna</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../style.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <h1>Witaj <?php echo $users['login']; ?></h1>
    <?php if ($users['icon'] != ""): ?>
        <td><img src= <?php echo $users['icon']; ?> width="200" height="200"></td>
    <?php else: ?>
        <img src="../download.jpg" width=200 height="200">
    <?php endif; ?>
    <p></p>
</div>
</body>

</html>