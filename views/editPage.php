<?php
require_once "../config.php";
if(!$perm->checkLogin()){
    header('Location: ../views/LoginPage.php');
    die;
}
$DAO = new UserDAO();
$result = $DAO->getUser($_GET['login']);

?>
<html>

<head>
    <title>Edycja</title>
    <link type="text/css" rel="stylesheet" href="../style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
</head>

<body>
<h3>Edycja konta</h3>
<div class="container">

    <form action="../Controllers/userController.php?option=edit&login=<?php echo $_GET['login'] ?>" method="POST">
        <label>Login:</label>
        <p>
            <input type="text" name="login" value="<?php echo $result->getLogin(); ?>" readonly><br>
        </p>
        <label>email:</label>
        <p>
            <input type="text" name="email" value="<?php echo $result->getEmail(); ?>"><br>
        </p>
        <?php if (!empty($_SESSION['error_email'])) {
            echo $_SESSION['error_email'];
            unset($_SESSION['error_email']);
        } ?>
        <label>wiek:</label>
        <p>
            <input type="text" name="age" value="<?php echo $result->getAge(); ?>"><br>
        </p>
        <label>telefon:</label>
        <p>
            <input type="text" name="phone" value="<?php echo $result->getPhone(); ?>"><br>
        </p>
        <?php if (!empty($_SESSION['error_phone'])) {
            echo $_SESSION['error_phone'];
            unset($_SESSION['error_phone']);
        } ?>
        <label>miejscowosc:</label>
        <p>
            <input type="text" name="city" value="<?php echo $result->getCity(); ?>"><br>
        </p>
        <label>zmień status:</label>
        <select id="status" name="status">
            <option value="offline">offline</option>
            <option value="online">online</option>
        </select><br>
        <label>Link do zdjęcia:</label>
        <p>
            <input type="text" name="icon"><br>
        </p>
        <input type="submit" class="btn btn-success my-2 my-sm-0"  value="edytuj"/>

    </form>
    <?php if (!empty($_SESSION['error'])) {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    } ?>

</div>
</body>

</html>