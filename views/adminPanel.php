<?php
require_once "../config.php";
if($perm->checkLogin()){
    if(!$perm->checkAdmin($_SESSION['user']['login'])){
        header('Location: ../views/mainPage.php');
        die;
    }
}else{
    header('Location: ../views/LoginPage.php');
    die;
}
$DAO = new UserDAO();
$API = new UserAPI();
$users = $DAO->getAll();
$usersAPI = $API->getAll();
$max = sizeof($usersAPI);

?>

<html>

<head>
    <title>Panel admina</title>
    <link type="text/css" rel="stylesheet" href="../style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <table id="admin" class="table my-2 my-sm-0">
        <thead class="thead-light">
        <tr>
            <th scope="col"></th>
            <th scope="col">Zdjęcie</th>
            <th scope="col">Login</th>
            <th scope="col">Status</th>
            <th scope="col">Akcje</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0;
        while ($i < $max): ?>
            <tr>
                <th scope="row"></th>
                <?php if ($usersAPI[$i]['icon'] != ""): ?>
                    <td><img src= <?php echo $usersAPI[$i]['icon']; ?> width="50" height="50"></td>
                <?php else: ?>
                    <td><img src="../download.jpg" width="50" height="50"></td>
                <?php endif; ?>
                <td><?php echo $usersAPI[$i]['login']; ?></td>
                <td><?php echo $usersAPI[$i]['status']; ?></td>
                <?php if (!$DAO->check($usersAPI[$i]['login']) or $perm->checkAdmin($usersAPI[$i]['login'])): ?>
                    <td>
                        <p>Nie można nic zrobić</p>
                    </td>
                <?php else: ?>
                    <td>

                        <a class="btn btn-success my-2 my-sm-0"
                           href="../Controllers/userController.php?option=delete&login=<?= $usersAPI[$i]['login'] ?>">usun</a>
                    </td>
                <?php endif; ?>

            </tr>
            <?php $i = $i + 1; endwhile; ?>
        </tbody>
    </table>
</div>

</body>

</html>