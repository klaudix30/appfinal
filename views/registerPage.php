<?php require_once "../config.php";
if($perm->checkLogin()){
    header('Location: ../views/mainPage.php');
    die;
}
?>
<html>

<head>
    <title>Rejestracja</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../style.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
</head>

<body>
<h3>Strona rejestracji</h3>
<div class="container">

    <form action="../Controllers/userController.php?option=register" method="POST">
        <label>Login:</label>
        <p>
            <input type="text" name="login" required/><br>
        </p>
        <label>Hasło:</label>
        <p>
            <input type="password" name="password" required/><br>
        </p>
        <label>Mail:</label>

        <p>
            <input type="mail" name="email" required/><br>
        </p>
        <?php if (!empty($_SESSION['error_email'])) {
            echo $_SESSION['error_email'];
            unset($_SESSION['error_email']);
        } ?>
        <label>Wiek:</label>
        <p>
            <input type="number" name="age" required/><br>
        </p>
        <label>Telefon:</label>
        <p>
            <input type="text" name="phone" required/><br>
        </p>
        <?php if (!empty($_SESSION['error_phone'])) {
            echo $_SESSION['error_phone'];
            unset($_SESSION['error_phone']);
        } ?>
        <label>Miejscowość:</label>
        <p>
            <input type="text" name="city" required/><br>
        </p>
        <input type="submit" class="btn btn-success my-2 my-sm-0" value="Zajerestruj"/>

    </form>
    <?php if (!empty($_SESSION['error'])) {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    } ?>
</div>
</body>

</html>
