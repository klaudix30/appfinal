<?php

require_once "../config.php";
if(!$perm->checkLogin()){
    header('Location: ../views/LoginPage.php');
    die;
}
$apiChat = new ChatAPI();
$array = $apiChat->getMessage();
$size = sizeof($array['list']);


?>

<html>

<head>
    <title>Ostatnie wiadomości</title>
    <link type="text/css" rel="stylesheet" href="../style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <a href="lastMessagePage.php?chat_id=<?php echo  $_GET['chat_id']; ?>">
        <input class="btn btn-success" style="float:left" type="submit" value="pokaz nowsze wiadomości"><br><br>
    </a>
    <table class="table my-2 my-sm-0">
        <thead class="thead-light">

        <tr>
            <th scope="col"></th>
            <th scope="col">Id</th>
            <th scope="col">Od kogo</th>
            <th scope="col">Wiadomość</th>
            <th scope="col">Data</th>
            <th scope="col">Id czatu</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0;
        foreach (array_reverse($array['list']) as $item):
            if($item['chat_id']==$_GET['chat_id']):
            ?>
            <tr>
                <th scope="row"></th>
                <td><?php echo $item['id'] ?></td>
                <td><?php echo $item['login']; ?></td>
                <td><?php echo $item['message'] ?></td>
                <td><?php echo $item['date']; ?></td>
                <td><?php echo $item['chat_id'] ?></td>
            </tr>
            <?php

            if ($i == $size - 1) {

                $_SESSION['lastMessage'] = $item['id'];
            }
            $i++;
            endif;
        endforeach;
        ?>
        </tbody>
    </table>


</div>
</body>

</html>
