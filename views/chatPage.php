<?php

require_once "../config.php";
if(!$perm->checkLogin()){
    header('Location: ../views/LoginPage.php');
    die;
}

$apiChat = new ChatAPI();
$array = $apiChat->getActive();
?>

<html>

<head>
    <title>Moje rozmowy</title>
    <link type="text/css" rel="stylesheet" href="../style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
</head>

<body>
<div class="container">

    <div class="w3-show-inline-block">
        <div class="w3-bar">
            <a href="createChatPage.php">
                <button class="btn btn-success">Utwórz rozmowe</button>
            </a>
            <a href="lastMessagePage.php">
                <button class="btn btn-success">Ostatnie wiadomości</button>
            </a>
        </div>
    </div>
    <br>
    <table class="table my-2 my-sm-0">
        <thead class="thead-light">

        <tr>
            <th scope="col"></th>
            <th scope="col">Id</th>
            <th scope="col">Nazwa</th>
            <th scope="col">Użytkownicy</th>
            <th scope="col">Akcje</th>
            <th scope="col">Dodaj użytkownika</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($array as $item): ?>
            <tr>
                <th scope="row"></th>
                <td><?php echo $item['id'] ?></td>
                <td><?php echo $item['name']; ?></td>
                <td><?php if ($item['users'] != NULL) {
                        foreach ($item['users'] as $user): echo $user; endforeach;
                    } ?></td>
                <td>

                    <a class="btn btn-success my-2 my-sm-0"
                       href="../Controllers/chatController.php?option=delete&id_chat=<?php echo $item['id'] ?>">Usuń</a>
                    <a class="btn btn-success my-2 my-sm-0"
                       href="chatSend.php?option=send&id_chat=<?php echo $item['id'] ?>">Wyślij wiadomość</a>
                    <a class="btn btn-success my-2 my-sm-0"
                       href="lastMessagePage.php?chat_id=<?php echo $item['id']?>">Pokaż</a>

                </td>
                <td>
                    <form name="form"
                          action="../Controllers/chatController.php?option=add&id_chat=<?php echo $item['id'] ?>"
                          method="POST">
                        <input type="text" name="userAdded" id="subject">
                        <input class="btn btn-primary" type="submit" value="dodaj"><br>
                    </form>
                    <?php if (!empty($_SESSION['error'])) {
                        echo $_SESSION['error'];
                        unset($_SESSION['error']);
                    } ?>


                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


</div>
</body>

</html>
