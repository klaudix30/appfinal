<?php
require_once "../config.php";

$user = new UserService();

if ($_GET["option"] === "register") {

    $data = array(
        'login' => trim($_POST['login']),
        'password' => trim($_POST['password']),
        'email' => trim($_POST['email']),
        'age' => trim($_POST['age']),
        'phone' => trim($_POST['phone']),
        'city' => trim($_POST['city']));
    if ($user->register($data)) {
        header('Location: ../views/loginPage.php');
    } else {
        $_SESSION['error'] = '<div class="error message">Nie można zajerestrować</div>';
        header('Location: ../views/registerPage.php');
    }
    die;
}
if ($_GET["option"] === "login") {
    $data = array(
        'login' => trim($_POST['login']),
        'password' => trim($_POST['password']));
    if ($user->login($data)) {
        header('Location: ../views/mainPage.php');
    } else {
        $_SESSION['error'] = '<div class="error message">Błędne dane</div>';
        header('Location: ../views/loginPage.php');
    }
    die;
}
if ($_GET["option"] === "delete") {

    if (!$perm->checkLogin()) {
        header('Location: views/loginPage.php');
        die;
    }
    if ($_GET['login'] == $_SESSION['user']['login']) {
        if (!$user->delete($_GET['login'])) {
            $_SESSION['error'] = '<div class="error message">Nie można usunąć</div>';
            header('Location: ../views/mainPage.php');
        } else {
            $_SESSION['error'] = '<div class="error message">Pomyślnie usunięto</div>';
            unset($_SESSION['user']);
            header('Location: ../views/loginPage.php');
        }
    } elseif ($perm->checkAdmin($_SESSION['user']['login'])) {

        if (!$user->delete($_GET['login'])) {
            $_SESSION['error'] = '<div class="error message">Nie można usunąć</div>';
            header('Location: views/mainPage.php');
        } else {
            header('Location: ../views/adminPanel.php');
        }
    }
    die;

}
if ($_GET["option"] === "edit") {


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $data = array(
            'email' => trim($_POST['email']),
            'age' => trim($_POST['age']),
            'phone' => trim($_POST['phone']),
            'city' => trim($_POST['city']));

        if ($user->edit($_GET['login'], $_SESSION['user']['key'], $data, $_POST['status'], trim($_POST['icon']))) {
            header('Location: ../views/mainPage.php');

        } else {
            $_SESSION['error'] = '<div class="error message">Nie można edytowac</div>';
            header('Location: ../views/editPage.php?login=' . $_GET['login']);
        }
        die;
    }
}
if ($_GET["option"] === "logout") {

    if (!$perm->checkLogin()) {
        header('Location: ../views/loginPage.php');
        die;
    }

    if($user->logout()) {
        $_SESSION['error']='<div class="error message">Pomyślnie wylogowano</div>';
        header('Location: ../views/loginPage.php');
        die;
    }
}
