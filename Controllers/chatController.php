<?php
require_once "../config.php";

$apiChat = new ChatAPI();

if ($_GET['option'] == 'delete') {
    if ($apiChat->leaveChat($_GET['id_chat'])) {

        header('Location: ../views/chatPage.php');
        die;
    }
}

if ($_GET['option'] == 'create') {
    if (!empty($_POST['nameChat'])) {
        if ($apiChat->createChat(trim($_POST['nameChat']))) {
            header('Location: ../views/chatPage.php');
        }
    } else {
        $_SESSION['error'] = '<div class="error message">Nazwa nie może być pusta</div>';
        header('Location: ../views/createChatPage.php');
    }
    die;
}


if ($_GET['option'] == 'send') {
    if (!empty($_POST['messageChat'])) {

        if ($apiChat->sendMessage($_GET['id_chat'], $_POST['messageChat'])) {

            header('Location: ../views/chatPage.php');

        } else {
            $_SESSION['error'] = '<div class="error message">Treść nie może być pusta</div>';
            header('Location: ../views/chatSend.php?option=send&id_chat=' . $_GET['id_chat'] . '');
        }
        die;

    }
}
if ($_GET['option'] == 'add') {
    $array = $apiChat->joinToChat($_POST['userAdded'], $_GET['id_chat']);
    if ($array == 0) {
        $_SESSION['error'] = '<div class="error message">Nie ma takiego</div>';
    }
    header('Location: ../views/chatPage.php');
    die;
}

