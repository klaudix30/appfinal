<?php

class UserService
{
    private $dao;
    private $api;
    private $validator;
    private $perm;

    public function __construct()
    {
        $this->dao = new UserDAO();
        $this->api = new UserAPI();
        $this->validator = new Validator();
        $this->perm = new Permission();
    }

    public function register($data)
    {

        if (!$this->validator->checkIfExist($data['login'])) {
            if ($this->validator->checkEmail($data['email']) and $this->validator->checkPhone($data['phone'])) {
                $hashedPassword = password_hash($data['password'], PASSWORD_BCRYPT);
                $this->api->setLogin($data['login']);
                if ($this->api->addUser($data['password'])) {
                    $result = $this->dao->add(new User($data['login'], $hashedPassword, $data['email'], $data['age'], $data['phone'], $data['city']));
                    if ($result) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function login($data)
    {

        $user = $this->dao->getUser($data['login']);
        if ($user) {
            if (password_verify($data['password'], $user->getPassword())) {
                $key = md5($data['login'] . hash('sha256', $data['password']));
                if (!isset($_SESSION['user']['key'])) {
                    $this->api->setKey($key);
                } else {
                    $this->api->setKey($_SESSION['user']['key']);
                }
                $this->api->setLogin($data['login']);
                if ($this->api->verify()) {
                    $_SESSION['user'] = array(
                        'login' => $_POST['login'],
                        'password' => $_POST['password'],
                        'key' => $key
                    );
                    return true;
                }
            }
        }

        return false;
    }

    public function delete($login)
    {
        $result = $this->dao->getUser($login);
        if ($result) {
            if ($this->dao->delete($login)) {
                return true;

            }
        }
        return false;

    }

    public function edit($login, $key, $data, $status, $icon)
    {
        if ($this->validator->checkEmail($data['email']) and $this->validator->checkPhone($data['phone'])) {
            $this->api->setLogin($login);
            $this->api->setKey($key);
            if ($this->api->editUser($status, $icon)) {
                $result = $this->dao->update($login, $data);
                if ($result) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
    public function logout()
    {
        $this->api->setLogin($_SESSION['user']['login']);
        $this->api->setKey($_SESSION['user']['key']);
        unset($_SESSION['user']);
        if($this->api->logout()){
            return true;
        }else{
            return false;
        }
    }


}