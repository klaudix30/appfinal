<?php

class Database
{

    public $db;

    function __construct()
    {

        $host = 'localhost:3307';
        $dbUser = 'root';
        $dbPass = '';
        $nameDb = 'phpcamp';
        $this->db = new mysqli($host, $dbUser, $dbPass, $nameDb);
        if ($this->db->connect_error) {
            var_dump($this->db->error_list);
            die;

        }


    }

    function __destruct()
    {

        $this->db->close();


    }

    public function getRow($query)
    {

        $result = $this->db->query($query);
        if ($result) {
            $return = $result->fetch_assoc();
            if ($return === null) {
                return [];
            } else {
                return $return;
            }
        } else {
            return [];
        }

    }

    public function getRows($query)
    {

        $result = $this->db->query($query);
        if ($result) {
            $rows = [];
            while (($row = $result->fetch_assoc())) {
                $rows[] = $row;
            }
            return $rows;

        } else {
            return [];
        }
    }

    public function query($query)
    {
        $result = $this->db->query($query);
        if ($result) {
            return true;

        } else {

            return false;
        }

    }

    public function getError()
    {
        return $this->db->error;

    }

    public function getListError()
    {
        return $this->db->error_list;

    }


}