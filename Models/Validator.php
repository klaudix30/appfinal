<?php


class Validator
{
    private $dao;


    public function __construct()
    {
        $this->dao = new UserDao();
    }

    public function checkIfExist($login)
    {

        $user = $this->dao->getUser($login);
        if ($user->getLogin() == $login) {
            $_SESSION['error'] = '<div class="error message">takie konto juz istnieje</div>';
            return true;
        } else {
            return false;
        }

    }

    public function checkEmail($email)
    {

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error_email'] = '<div class="error message">Błędny mail</div>';
            return false;
        } else {
            return true;
        }
    }

    public function checkPhone($phone)
    {

        $phoneFiltered = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
        $phoneChecked = str_replace("-", "", $phoneFiltered);
        if (strlen($phoneChecked) < 10 || strlen($phoneChecked) > 14) {
            $_SESSION['error_phone'] = '<div class="error message">Błędny numer telefonu</div>';
            return false;
        } else {
            return true;

        }
    }


}