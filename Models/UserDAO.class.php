<?php
//require_once "Database.class.php";
//require_once "User.class.php";
class UserDao
{
    private $db;

    /**
     * @return Database
     */
    public function getDb(): Database
    {
        return $this->db;
    }


    public function __construct()
    {

        $this->db = new Database();
    }

    public function getAll(): array
    {

        $query = 'SELECT *FROM `users`';
        $rows = $this->db->getRows($query);
        $usersObjects = [];
        foreach ($rows as $row) {

            $usersObjects[] = $this->getUserObject($row);
        }
        return $usersObjects;
    }

    public function getUser($login)
    {
        $query = 'SELECT *FROM users WHERE login="' . $login . '"';

        $row = $this->db->getRow($query);

        return $this->getUserObject($row);

    }

    public function getUserObject(array $row): User
    {

        return new User($row['login'], $row['password'], $row['email'], $row['age'], $row['phone'], $row['city']);

    }

    public function add(User $user)
    {

        $query = "Insert into `users`(`login`,`password`,`email`,`age`,`phone`,`city`)
		Values('{$user->getLogin()}','{$user->getPassword()}','{$user->getEmail()}','{$user->getAge()}','{$user->getPhone()}','{$user->getCity()}');";

        return $this->db->query($query);
    }

    public function delete($login)
    {
        $query = "DELETE from `users` where login='$login'";
        $result_delete = $this->db->query($query);
        if ($result_delete) {
            return true;
        } else {
            return false;
        }
    }

    public function update($login, $fields)
    {
        $email = $fields['email'];
        $age = $fields['age'];
        $phone = $fields['phone'];
        $city = $fields['city'];

        $query = "Update users set email='$email', age='$age', phone='$phone', city='$city' where login='$login' ";
        $result_update = $this->db->query($query);
        if ($result_update) {
            return true;
        } else {
            return false;
        }
    }

    public function check($login)
    {
        $query = 'SELECT *FROM users WHERE login="' . $login . '"';

        $row = $this->db->getRow($query);
        if ($row) {
            return true;
        } else {
            return false;
        }
    }




}