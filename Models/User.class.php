<?php

class User
{

    private $login;
    private $password;
    private $email;
    private $age;
    private $phone;
    private $city;

    /**
     * @return mixed
     */


    /**
     * @param mixed $permissions
     */


    /**
     * User constructor.
     * @param $login
     * @param $password
     * @param $email
     * @param $age
     * @param $phone
     * @param $city
     */


    public function __construct($login, $password, $email, $age, $phone, $city)
    {
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;
        $this->age = $age;
        $this->phone = $phone;
        $this->city = $city;


    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }


}