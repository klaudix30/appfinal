<?php


class Permission
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    function checkLogin()
    {
        if (empty($_SESSION['user'])) {
            return false;
        } else {
            return true;
        }
    }

    public function checkAdmin($login)
    {
        if (isset($login)) {
            $query = 'SELECT permissions from users where login="' . $login . '"';
            $result = $this->db->getRow($query);
            if ($result['permissions'] == '1') {
                return false;

            } else {
                return true;
            }

        }
    }

}